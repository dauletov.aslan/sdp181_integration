package kz.test.integrationexample;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openFacebook(View view) {
        String packageName = "com.facebook.katana";
        String className = "com.facebook.katana.LoginActivity";
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            Intent intent = new Intent("android.intent.category.LAUNCHER");
            intent.setClassName(packageName, className);
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            Uri uri = Uri.parse("market://details?id=" + packageName);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    }

    public void volley(View view) {
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        String url = "https://jsonplaceholder.typicode.com/posts";
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        Log.d("Hello", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
        requestQueue.add(stringRequest);
    }

    public void retrofit(View view) {
        startActivity(new Intent(MainActivity.this, RetrofitActivity.class));
    }

    public void loginWithTelegram(View view) {
//        TelegramPassport.AuthRequest request = new TelegramPassport.AuthRequest();
//        request.botID = 1463386154;
//        request.publicKey = "1463386154:AAG_ONt_Xcrxf6y6dKgq-DEY9eeVuH84-v0";
//        request.nonce = /* a unique nonce to pass to the bot server */;
//        request.scope = new PassportScope(
//                new PassportScopeElementOneOfSeveral(PassportScope.PASSPORT, PassportScope.IDENTITY_CARD).withSelfie(),
//                new PassportScopeElementOne(PassportScope.PERSONAL_DETAILS).withNativeNames(),
//                PassportScope.DRIVER_LICENSE,
//                PassportScope.ADDRESS,
//                PassportScope.ADDRESS_DOCUMENT,
//                PassportScope.PHONE_NUMBER
//        );
//        TelegramPassport.request(MainActivity.this, request, 100);
    }

    public void whatsapp(View view) {
        String packageName = "com.whatsapp";
        try {
            getPackageManager().getApplicationInfo(packageName, 0);
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.setType("text/plain");
            sendIntent.setPackage(packageName);
            startActivity(sendIntent);
        } catch (PackageManager.NameNotFoundException e) {
            Uri uri = Uri.parse("market://details?id=" + packageName);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }
    }

    public void loginWithFacebook(View view) {
    }
}