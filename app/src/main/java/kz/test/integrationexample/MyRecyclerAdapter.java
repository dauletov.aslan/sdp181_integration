package kz.test.integrationexample;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ItemViewHolder> {

    private List<Post> list;

    public MyRecyclerAdapter(List<Post> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.idText.setText(list.get(position).getId());
        holder.userIdTextView.setText(list.get(position).getUserId());
        holder.titleTextView.setText(list.get(position).getTitle());
        holder.bodyTextView.setText(list.get(position).getBody());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setItems(List<Post> items) {
        list.clear();
        list.addAll(items);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView idText, userIdTextView, titleTextView, bodyTextView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            idText = itemView.findViewById(R.id.idTextView);
            userIdTextView = itemView.findViewById(R.id.userIdTextView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            bodyTextView = itemView.findViewById(R.id.bodyTextView);
        }
    }
}
